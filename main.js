const sentMessages = document.querySelector("#sentMessages");
const url = "http://treinamento-ajax-api.herokuapp.com"



function createMessage(txt = "sample", author = "author", id=""){
    
    const messageContainer = document.createElement("li");
    const message = document.createElement("p");
    const authorName = document.createElement("p");
    const buttonArea = document.createElement("div");
    const editField = document.createElement("div");

    authorName.innerText = `Autor: ${author}`;
    authorName.className = "author-name";
    messageContainer.className = "sent-message-container";
    messageContainer.id = id
    message.className = "sent-message"
    message.innerText = txt;
    buttonArea.className = "button-area";
    editField.className = "edit-field";

    messageContainer.appendChild(authorName);
    messageContainer.appendChild(message);
    buttonArea.appendChild(createButton("Deletar", "red", deleteMessage));
    buttonArea.appendChild(createButton("Editar", "green", editMessage));
    editField.appendChild(document.createElement("input"));
    editField.appendChild(createButton("Enviar", "teal", sendEdit))
    buttonArea.appendChild(editField);
    messageContainer.appendChild(buttonArea);
    sentMessages.appendChild(messageContainer);
}

function createButton(txt = "", color = "black", callback){
    const button = document.createElement("button");
    button.innerText = txt;
    button.className = "button-base";

    button.style.backgroundColor = color;
    button.addEventListener("click", ()=>{
        callback(button);
    });
    return button;
}

function deleteMessage(btn){
    const message = btn.parentNode.parentNode
    const config = {
        method: "DELETE",
    };

    fetch(url + "/messages/" + message.id, config)
    .then(()=>{
        message.remove();
    })
    .catch((err) => {
        console.log(err);
    })
}

function getMessages(){
    fetch(url + "/messages")
    .then((response => response.json()))
    .then(messages => {
        messages.forEach(element => {
            createMessage(element.content, element.author, element.id);
        });
    })
    .catch((err) => {
        console.log(err);
    })
}


function editMessage(button){
    const editField = button.parentNode.querySelector(".edit-field");
    const originalMessage = editField.parentNode.parentNode
    console.log(originalMessage);
    editField.style.display = "block";
    console.log(`${originalMessage.querySelector(".sent-message").innerText}`)
    editField.querySelector("input").value = `${originalMessage.querySelector(".sent-message").innerText}`;
}

function postMessage(){
    const txt = document.querySelector("#message").value;
    const author = document.querySelector("#author").value;

    const body = {
        content: txt,
        author: author
    }

    const config = {
        method: "POST",
        body: JSON.stringify(body),
        headers: {"Content-type": "application/json"},
    }

    fetch(url+"/messages", config)
    .then((response => response.json()))
    .then(element => {       
            createMessage(element.content, element.author, element.id);
    })
    .catch((err) => {
        console.log(err);
    });
}

function sendEdit(btn){
    const message = btn.parentNode.parentNode.parentNode;
    const newText = btn.parentNode.querySelector("input").value;
    btn.parentNode.style.display = "none" //esconde o edit field
    console.log(newText);
    body = {
        content: `${newText}`
    }

    const config = {
        method: "PATCH",
        body: JSON.stringify(body),
        headers: {"Content-type": "application/json"},
    }

    console.log(message.id);
    fetch(url + "/messages/" + message.id, config)
    .then((response => response.json()))
    .then(() => {       
            message.querySelector(".sent-message").innerText = newText;
    })
    .catch((err) => {
        console.log(err);
    });
}

getMessages();